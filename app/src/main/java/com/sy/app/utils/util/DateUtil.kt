package com.sy.app.utils.util

import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat


object DateUtil {
    var dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    fun stringToLocalDateTime(string: String?): LocalDateTime? {
        if (string.isNullOrEmpty())
            return null

        return dateTimeFormatter.parseLocalDateTime(string)
    }

    fun localDateTimeToString(localDateTime: LocalDateTime?): String {
        if (localDateTime == null)
            return ""

        return dateTimeFormatter.print(localDateTime)
    }
}
