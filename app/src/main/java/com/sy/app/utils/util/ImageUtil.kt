package com.sy.app.utils.util

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.provider.MediaStore
import com.sy.app.utils.MLog
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


object ImageUtil {
    fun compressImageFile(imageFile: File): File {

        MLog.d("Origin Image File Size : ${imageFile.length()}")

        var outputStream: FileOutputStream? = null
        val imagePath = imageFile.absolutePath
        val extension = imagePath.substring(imagePath.lastIndexOf(".") + 1)

        val option = BitmapFactory.Options()
        val src = BitmapFactory.decodeFile(imageFile.absolutePath, option)
        var resizedImage = Bitmap.createScaledBitmap(src!!, src.width, src.height, true)

        val exif = ExifInterface(imageFile.absolutePath)
        val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)
        val degree = exifOrientationToDegrees(orientation)
        resizedImage = imageRotate(resizedImage, degree)

        val compressType = if (extension.equals("png", ignoreCase = true)) {
            Bitmap.CompressFormat.PNG
        } else {
            Bitmap.CompressFormat.JPEG
        }

        try {
            outputStream = FileOutputStream(imageFile)

            resizedImage.compress(compressType, 70, outputStream)
        } catch (e: IOException) {
            MLog.e("Image Resizing Error : " + e.toString())
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close()
                } catch (e: IOException) {
                    MLog.e("OutputStream Close Fail : " + e.toString())
                }
            }

            resizedImage?.recycle()
            src.recycle()
        }

        MLog.d("Compress Image File Size : ${imageFile.length()}")

        return imageFile
    }

    private fun imageRotate(origin: Bitmap?, degrees: Int): Bitmap? {
        var bitmap = origin
        if (degrees != 0 && bitmap != null) {
            val m = Matrix()
            m.setRotate(degrees.toFloat(), bitmap.width.toFloat() / 2,
                    bitmap.height.toFloat() / 2)

            try {
                val converted = Bitmap.createBitmap(bitmap, 0, 0,
                        bitmap.width, bitmap.height, m, true)
                if (bitmap != converted) {
                    bitmap.recycle()
                    bitmap = converted
                }
            } catch (ex: OutOfMemoryError) {
                // 메모리가 부족하여 회전을 시키지 못할 경우 그냥 원본을 반환합니다.
            }

        }
        return bitmap
    }

    private fun exifOrientationToDegrees(exifOrientation: Int): Int {
        return when (exifOrientation) {
            ExifInterface.ORIENTATION_ROTATE_90 -> 90
            ExifInterface.ORIENTATION_ROTATE_180 -> 180
            ExifInterface.ORIENTATION_ROTATE_270 -> 270
            else -> 0
        }
    }
}

fun Context.insertImageToMediaStore(imageFile: File) {
    val contentResolver = contentResolver
    val imagePath = imageFile.absolutePath

    val contentValues = ContentValues()
    val fileName = imagePath.substring(imagePath.lastIndexOf("/") + 1)
    contentValues.put(MediaStore.Files.FileColumns.TITLE, fileName)
    contentValues.put(MediaStore.Files.FileColumns.DISPLAY_NAME, fileName)
    contentValues.put(MediaStore.Files.FileColumns.DATE_ADDED, System.currentTimeMillis())
    contentValues.put(MediaStore.Files.FileColumns.DATA, imageFile.absolutePath)

    contentResolver.insert(MediaStore.Files.getContentUri("external"), contentValues)

}