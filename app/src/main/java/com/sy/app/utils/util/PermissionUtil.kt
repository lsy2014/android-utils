package com.sy.app.utils.util

import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * Created by intobook on 2018. 1. 9..
 */

fun RxPermissions.checkAndRequestPermission(permissions: Array<String>): Observable<Boolean> {
    return if (permissions.all { isGranted(it) }) {
        Observable.just(true)
    } else {
        request(*permissions)
    }
}

fun RxPermissions.checkAndRequestPermission(permissions:Array<String>, ifEnableFun:()-> Unit) {
    checkAndRequestPermission(permissions, ifEnableFun, null)
}

fun RxPermissions.checkAndRequestPermission(permissions:Array<String>, ifEnableFun: () -> Unit, ifDisableFun: (() -> Unit)?) {
    if (permissions.all { isGranted(it) }) {
        ifEnableFun()
    } else {
        request(*permissions)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({isAllEnable->
                    if (isAllEnable) ifEnableFun()
                    else ifDisableFun?.invoke()
                })
    }
}

private fun RxPermissions.isAllGranted(vararg permissions: String): Boolean {
    return permissions.all { isGranted(it) }
}