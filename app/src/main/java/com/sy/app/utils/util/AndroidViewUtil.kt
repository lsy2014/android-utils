package com.sy.app.utils.util

import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView

fun TextView.addOnTextChanged(textChanged: (CharSequence?, Int, Int, Int) -> Unit) {
    addTextChangedListener(object: TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            textChanged(s, start, before, count)
        }
    })
}

fun ViewPager.addOnPageSelected(pageSelected: (Int) -> Unit) {
    addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {

        }

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        }

        override fun onPageSelected(position: Int) {
            pageSelected(position)
        }
    })
}