package com.sy.app.utils

import android.util.Log

/**
 * Created by intobook on 2017. 10. 25..
 */

object MLog {
    private val TAG = "GoodRoad"
    var debuggable = false

    /** Log Level Error  */
    fun e(message: String) {
        if (debuggable) {
            Log.e(TAG, buildLogMsg(message))
        }
    }

    fun e(throwable: Throwable) {
        e(Log.getStackTraceString(throwable))
    }

    /** Log Level Warning  */
    fun w(message: String) {
        if (debuggable) {
            Log.w(TAG, buildLogMsg(message))
        }
    }

    /** Log Level Information  */
    fun i(message: String) {
        if (debuggable) {
            Log.i(TAG, buildLogMsg(message))
        }
    }

    /** Log Level Debug  */
    fun d(message: String) {
        if (debuggable) {
            Log.d(TAG, buildLogMsg(message))
        }
    }

    /** Log Level Verbose  */
    fun v(message: String) {
        if (debuggable) {
            Log.v(TAG, buildLogMsg(message))
        }
    }

    private fun buildLogMsg(message: String?): String {
        try {
            if (message != null) {
                val ste = Thread.currentThread().stackTrace[4]

                val sb = StringBuilder()

                sb.append("[")
                sb.append(ste?.fileName?.replace(".java", ""))
                sb.append("::")
                sb.append(ste.methodName)
                sb.append("]")
                sb.append(message)

                return sb.toString()
            } else {
                return ""
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""

    }

}
