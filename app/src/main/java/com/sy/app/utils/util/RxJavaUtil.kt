package com.sy.app.utils.util

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by intobook on 2018. 1. 22..
 */

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}