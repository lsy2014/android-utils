package com.sy.app.utils.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.telephony.TelephonyManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@SuppressLint("MissingPermission", "HardwareIds")
fun Context.getDevicePhoneNumber(): String? {
    var phoneNumber: String? = ""

    val packageManager = packageManager
    if (packageManager != null && packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
        phoneNumber = (getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager).line1Number

        if (phoneNumber?.contains("+82") == true) {
            phoneNumber = phoneNumber.replace("+82", "0")
        }
    }

    return phoneNumber
}

fun Context.openWebBrowser(url: String?) {
    if (url == null || url.isEmpty())
        return

    var webUrl = url

    if (!webUrl.startsWith("http://") && !webUrl.startsWith("https://"))
        webUrl = "http://$webUrl"

    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(webUrl)))
}

inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)!!
