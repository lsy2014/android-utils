package com.sy.app.utils.util

import android.location.LocationManager

/**
 * Created by intobook on 2017. 10. 27..
 */

object LocationUtil {
    val INTERVAL = 2000
    val FAST_INTERVAL = 1000

    fun hasLocationProvider(locationManager: LocationManager?): Boolean {
        return locationManager != null &&
                (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                 locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
    }
}
